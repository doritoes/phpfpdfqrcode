# phpfpdfqrcode

Draw QR codes in PDF files using PHP with fpdf and phpqrcode

Built with fdpf v1.82 (2019-12-07) and phpqrcode v1.1.4 build 2010100721

# Applications
## QR wifi codes
Create PDFs for signs, posters, business cards, etc. with QR codes that connect to wifi

## Registration codes
Create PDFs for on-line products that can be scanned to open information customized to the user

# Setup
1. Download fpdf from http://www.fpdf.org/en/download.php
2. Expand the zip file to your web server directory
3. Clone/download phpqrcode from https://github.com/t0k4rt/phpqrcode to a subfolder
4. Put qrpdf.php (this repository) in the web server directory