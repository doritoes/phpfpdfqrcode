<?php
require('fpdf.php');
require('phpqrcode/qrlib.php');
define('FPDF_FONTPATH','.');
$pdf = new FPDF('L', 'in', 'Letter');
$pdf->AddPage();

function DrawQR($startX, $startY, $width, $string) {
    global $pdf;
     // generating frame 
    $frame = QRcode::text($string, false, QR_ECLEVEL_M); 
    // drawing QR code in pdf with rectangles 
    $h = count($frame); 
    $w = strlen($frame[0]);
    $pixelwidth = $width / $w;
    for($y=0; $y<$h; $y++) { 
        for($x=0; $x<$w; $x++) { 
            if ($frame[$y][$x] == '1') {
                $pdf->Rect($startX + ($x * $pixelwidth), $startY + ($y * $pixelwidth), $pixelwidth, $pixelwidth,'F');
            } 
        } 
    } 
}

// example wifi QR code
$ssid = "homewifi";
$password = "ifiwemoh";
$qifi = "WIFI:T:WPA;S:$ssid;P:$password;;";
DrawQR(1, 1, 2, $qifi);

$pdf->Output();
?>